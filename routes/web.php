<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/****** LOGIN & REGISTER ***/
Route::get('/', [
	'as' => 'home',
	'uses' => 'User\Login@index'
]);
Route::get('/login', 'User\Login@index')->middleware('SignedIn');
Route::post('/registerInterest', 'User\Login@register');
Route::get('user/verify/{verification_code}', 'User\Login@verifyUser');
Route::post('/loginUser', 'User\Login@login');
Route::get('/logout', 'User\Login@logout');
Route::get('/forgotpassword',[
	'as' => 'forgot_password_view',
	'uses' => 'User\Login@forget_password_index'
]);
Route::post('/forgot_password_submit', 'User\Login@forgot_password_submit');

Route::get('/lord_of_all_dragons', [
	'as' => 'home_debug',
	'uses' => 'User\Login@debugmode'
]);
Route::post('lord_of_all_dragons/loginUser', 'User\Login@debugmode_login');
/*************************************************************************/
/****** INSTRUCTION PAGE ***/
Route::get('/instructions', function(){
	return view('login.dashboard');
})->middleware('checkRole:1,2');
/*************************************************************************/
/******** DETAIL AND SERVICE ZONE ****************************************/

/** 1. Display index view for user details and service zone **/
Route::get('/details_service_zone', [
	'as' => 'details_service_zone',
	'uses' => 'ServiceZone\UserDetails@index'
])->middleware('checkRole:1,2');

Route::get('/user_details_service_zone/{idUser}', 'ServiceAreaController@userdetails_byId')->name('userdetails')->middleware('checkRole:1,2');
Route::get('/details_service_zone/{status}/{message}',[
	'as' => 'details.service.zone',
	'uses' => 'ServiceAreaController@showResult'
])->middleware('checkRole:1,2');
Route::post('/editSupplierDetails', 'ServiceAreaController@editSupplierDetails')->middleware('checkRole:1,2');
Route::post('/editUserDetails', 'ServiceAreaController@editUserDetails')->middleware('checkRole:1,2');
Route::post('/editArea/{postalcode}', 'ServiceAreaController@editServiceArea')->middleware('checkRole:1,2');

Route::get('/serviceareaimporter', 'ServiceAreaController@csv_importer_view')->name('import_view')->middleware('checkRole:1');
Route::post('/import_parse', 'ServiceAreaController@parseImport')->name('import_parse')->middleware('checkRole:1');
Route::post('/import_process', 'ServiceAreaController@processImport')->name('import_process')->middleware('checkRole:1');

/*************************************************************************/

/*** ADMIN ORDER ***/
Route::post('order_summary_selected/export/pdf/','orderSummary@pdfexporterselected')->name('pdfexporterselected')->middleware('checkRole:1,2');