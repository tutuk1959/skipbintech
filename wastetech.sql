-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2020 at 05:29 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wastetech`
--

-- --------------------------------------------------------

--
-- Table structure for table `csv_data`
--

CREATE TABLE `csv_data` (
  `id` int(11) NOT NULL,
  `csv_filename` varchar(255) DEFAULT NULL,
  `csv_header` tinyint(1) DEFAULT NULL,
  `csv_data` longtext DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2018_06_11_052427_create_bin_service', 1),
(3, '2018_06_11_053118_create_bin_service_updates', 1),
(4, '2018_06_11_053255_create_bin_type', 1),
(5, '2018_06_11_054938_create_supplier_table', 1),
(6, '2018_06_11_055545_create_service_area', 1),
(7, '2018_06_11_055941_create_size_table', 1),
(8, '2018_06_11_060117_create_order_service_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinnondelivery`
--

CREATE TABLE `tblbinnondelivery` (
  `idNonDeliveryDays` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  `idSupplier` int(10) DEFAULT NULL,
  `idUser` int(10) DEFAULT NULL,
  `idBinType` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbinnondelivery`
--

INSERT INTO `tblbinnondelivery` (`idNonDeliveryDays`, `date`, `idSupplier`, `idUser`, `idBinType`) VALUES
(21, '2019-04-06', 11, 5, 5),
(22, '2019-04-07', 11, 5, 5),
(23, '2019-04-13', 11, 5, 5),
(24, '2019-04-14', 11, 5, 5),
(221, '2019-10-12', 11, 5, 1),
(222, '2019-10-12', 11, 5, 2),
(223, '2019-10-12', 11, 5, 3),
(224, '2019-10-12', 11, 5, 4),
(225, '2019-10-12', 11, 5, 5),
(226, '2019-10-13', 11, 5, 1),
(227, '2019-10-13', 11, 5, 2),
(228, '2019-10-13', 11, 5, 3),
(229, '2019-10-13', 11, 5, 4),
(230, '2019-10-13', 11, 5, 5),
(231, '2019-10-19', 11, 5, 1),
(232, '2019-10-19', 11, 5, 2),
(233, '2019-10-19', 11, 5, 3),
(234, '2019-10-19', 11, 5, 4),
(235, '2019-10-19', 11, 5, 5),
(236, '2019-10-20', 11, 5, 1),
(237, '2019-10-20', 11, 5, 2),
(238, '2019-10-20', 11, 5, 3),
(239, '2019-10-20', 11, 5, 4),
(240, '2019-10-20', 11, 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinservice`
--

CREATE TABLE `tblbinservice` (
  `idBinService` int(10) UNSIGNED NOT NULL,
  `idSupplier` int(11) NOT NULL,
  `idBinType` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `default_stock` int(11) NOT NULL,
  `idBinSize` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbinservice`
--

INSERT INTO `tblbinservice` (`idBinService`, `idSupplier`, `idBinType`, `price`, `stock`, `default_stock`, `idBinSize`) VALUES
(22, 11, 1, 130.00, 0, 10, 1),
(23, 11, 1, 130.00, 0, 5, 2),
(24, 11, 2, 130.00, 4, 10, 1),
(25, 11, 4, 250.00, 0, 2, 1),
(26, 11, 5, 44.00, 8, 10, 1),
(57, 11, 4, 222.00, 0, 1, 2),
(166, 11, 4, 0.00, 0, 0, 6),
(168, 11, 5, 0.00, 0, 0, 2),
(169, 11, 5, 121.00, 0, 1, 3),
(170, 11, 5, 128.00, 0, 1, 4),
(171, 11, 2, 150.00, 5, 10, 2),
(172, 11, 2, 130.00, 9, 10, 3),
(173, 11, 2, 130.00, 10, 10, 5),
(174, 11, 2, 130.00, 8, 8, 6),
(175, 11, 1, 130.00, 7, 10, 6),
(176, 11, 1, 130.00, 8, 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceoptions`
--

CREATE TABLE `tblbinserviceoptions` (
  `idBinServiceOptions` int(12) NOT NULL,
  `idUser` int(12) DEFAULT NULL,
  `idSupplier` int(12) DEFAULT NULL,
  `idBinType` int(12) DEFAULT NULL,
  `extraHireagePrice` bigint(20) DEFAULT NULL,
  `extraHireageDays` int(11) DEFAULT NULL,
  `excessWeightPrice` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbinserviceoptions`
--

INSERT INTO `tblbinserviceoptions` (`idBinServiceOptions`, `idUser`, `idSupplier`, `idBinType`, `extraHireagePrice`, `extraHireageDays`, `excessWeightPrice`) VALUES
(45, 5, 11, 1, 60, 7, 30),
(46, 5, 11, 2, 12, 7, 0),
(47, 5, 11, 4, 12, 7, 200),
(48, 5, 11, 5, 12, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblbinservicestok`
--

CREATE TABLE `tblbinservicestok` (
  `idBinServiceStok` int(12) NOT NULL,
  `idBinService` int(12) NOT NULL,
  `stock` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbinservicestok`
--

INSERT INTO `tblbinservicestok` (`idBinServiceStok`, `idBinService`, `stock`, `date`) VALUES
(907, 22, 0, '2020-02-15'),
(908, 22, 9, '2020-02-16'),
(909, 23, 4, '2020-02-17'),
(910, 23, 3, '2020-02-18'),
(911, 23, 3, '2020-02-19'),
(912, 23, 2, '2020-02-20'),
(913, 23, 2, '2020-02-21'),
(914, 23, 1, '2020-02-15'),
(915, 23, 1, '2020-02-16'),
(916, 24, 0, '2020-02-24'),
(917, 24, 0, '2020-02-25'),
(918, 23, 0, '2020-03-02'),
(919, 23, 0, '2020-03-03'),
(920, 22, 8, '2020-02-17'),
(921, 22, 8, '2020-02-18'),
(922, 22, 0, '2020-02-19'),
(923, 22, 0, '2020-02-20'),
(924, 22, 0, '2020-02-21'),
(925, 22, 0, '2020-02-22'),
(926, 22, 0, '2020-02-23'),
(927, 22, 0, '2020-02-24'),
(928, 22, 0, '2020-02-25'),
(929, 22, 0, '2020-02-26'),
(930, 22, 0, '2020-02-27'),
(931, 22, 0, '2020-02-28'),
(932, 24, 1, '2020-02-15'),
(933, 24, 1, '2020-02-16'),
(934, 24, 1, '2020-02-17'),
(935, 24, 1, '2020-02-18'),
(936, 24, 5, '2020-02-19'),
(937, 24, 5, '2020-02-20'),
(938, 24, 1, '2020-02-21'),
(939, 24, 1, '2020-02-22'),
(940, 24, 1, '2020-02-23'),
(941, 24, 1, '2020-02-26'),
(942, 24, 1, '2020-02-27'),
(943, 24, 1, '2020-02-28'),
(944, 24, 9, '2020-03-11'),
(945, 24, 9, '2020-03-12'),
(946, 171, 9, '2020-03-09'),
(947, 171, 9, '2020-03-10'),
(948, 171, 9, '2020-03-11'),
(949, 171, 8, '2020-02-16'),
(950, 171, 8, '2020-02-17'),
(951, 22, 5, '2020-03-02'),
(952, 22, 5, '2020-03-03'),
(953, 26, 8, '2020-02-19'),
(954, 26, 8, '2020-02-20'),
(955, 171, 5, '2020-02-19'),
(956, 171, 5, '2020-02-20'),
(957, 175, 7, '2020-02-19'),
(958, 175, 7, '2020-02-20'),
(959, 57, 0, '2020-02-19'),
(960, 57, 0, '2020-02-20'),
(961, 172, 9, '2020-02-19'),
(962, 172, 9, '2020-02-20'),
(963, 172, 9, '2020-02-21'),
(964, 172, 9, '2020-02-22'),
(965, 172, 9, '2020-02-23'),
(966, 172, 9, '2020-02-24'),
(967, 172, 9, '2020-02-25'),
(968, 172, 9, '2020-02-26'),
(969, 172, 9, '2020-02-27'),
(970, 172, 9, '2020-02-28'),
(971, 172, 9, '2020-02-29'),
(972, 22, 0, '2020-02-29'),
(973, 169, 0, '2020-02-19'),
(974, 169, 0, '2020-02-20'),
(975, 169, 0, '2020-02-21'),
(976, 169, 0, '2020-02-22'),
(977, 176, 8, '2020-02-19'),
(978, 176, 8, '2020-02-20'),
(979, 170, 0, '2020-02-19'),
(980, 170, 0, '2020-02-20'),
(981, 25, 0, '2020-02-19'),
(982, 25, 0, '2020-02-20'),
(983, 24, 4, '2020-03-04'),
(984, 24, 4, '2020-03-05'),
(985, 24, 4, '2020-03-06'),
(986, 24, 4, '2020-03-07'),
(987, 24, 4, '2020-03-08'),
(988, 24, 4, '2020-03-09');

-- --------------------------------------------------------

--
-- Table structure for table `tblbinserviceupdates`
--

CREATE TABLE `tblbinserviceupdates` (
  `idBinServiceUpdates` int(10) UNSIGNED NOT NULL,
  `idBinService` int(11) NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbinserviceupdates`
--

INSERT INTO `tblbinserviceupdates` (`idBinServiceUpdates`, `idBinService`, `price`, `stock`, `date`) VALUES
(1024, 22, 130.00, 0, '2020-02-15'),
(1025, 22, 130.00, 9, '2020-02-16'),
(1026, 23, 130.00, 4, '2020-02-17'),
(1027, 23, 130.00, 3, '2020-02-18'),
(1028, 23, 130.00, 3, '2020-02-19'),
(1029, 23, 130.00, 2, '2020-02-20'),
(1030, 23, 130.00, 2, '2020-02-21'),
(1031, 23, 130.00, 1, '2020-02-15'),
(1032, 23, 130.00, 1, '2020-02-16'),
(1033, 24, 130.00, 0, '2020-02-24'),
(1034, 24, 130.00, 0, '2020-02-25'),
(1035, 23, 130.00, 0, '2020-03-02'),
(1036, 23, 130.00, 0, '2020-03-03'),
(1037, 24, 130.00, 1, '2020-02-15'),
(1038, 24, 130.00, 1, '2020-02-16'),
(1039, 24, 130.00, 1, '2020-02-17'),
(1040, 24, 130.00, 1, '2020-02-18'),
(1041, 24, 130.00, 5, '2020-02-19'),
(1042, 24, 130.00, 5, '2020-02-20'),
(1043, 24, 130.00, 1, '2020-02-21'),
(1044, 24, 130.00, 1, '2020-02-22'),
(1045, 24, 130.00, 1, '2020-02-23'),
(1046, 24, 130.00, 1, '2020-02-26'),
(1047, 24, 130.00, 1, '2020-02-27'),
(1048, 24, 130.00, 1, '2020-02-28'),
(1049, 24, 130.00, 9, '2020-03-11'),
(1050, 24, 130.00, 9, '2020-03-12'),
(1051, 171, 150.00, 9, '2020-03-09'),
(1052, 171, 150.00, 9, '2020-03-10'),
(1053, 171, 150.00, 9, '2020-03-11'),
(1054, 171, 150.00, 8, '2020-02-16'),
(1055, 171, 150.00, 8, '2020-02-17'),
(1056, 22, 130.00, 8, '2020-02-17'),
(1057, 22, 130.00, 8, '2020-02-18'),
(1058, 22, 130.00, 0, '2020-02-19'),
(1059, 22, 130.00, 0, '2020-02-20'),
(1060, 22, 130.00, 5, '2020-03-02'),
(1061, 22, 130.00, 5, '2020-03-03'),
(1062, 26, 44.00, 8, '2020-02-19'),
(1063, 26, 44.00, 8, '2020-02-20'),
(1064, 171, 150.00, 5, '2020-02-19'),
(1065, 171, 150.00, 5, '2020-02-20'),
(1066, 175, 130.00, 7, '2020-02-19'),
(1067, 175, 130.00, 7, '2020-02-20'),
(1068, 57, 222.00, 0, '2020-02-19'),
(1069, 57, 222.00, 0, '2020-02-20'),
(1070, 172, 166.00, 9, '2020-02-19'),
(1071, 172, 166.00, 9, '2020-02-20'),
(1072, 172, 166.00, 9, '2020-02-21'),
(1073, 172, 166.00, 9, '2020-02-22'),
(1074, 172, 166.00, 9, '2020-02-23'),
(1075, 172, 166.00, 9, '2020-02-24'),
(1076, 172, 166.00, 9, '2020-02-25'),
(1077, 172, 166.00, 9, '2020-02-26'),
(1078, 172, 166.00, 9, '2020-02-27'),
(1079, 172, 166.00, 9, '2020-02-28'),
(1080, 172, 166.00, 9, '2020-02-29'),
(1081, 22, 310.00, 0, '2020-02-21'),
(1082, 22, 310.00, 0, '2020-02-22'),
(1083, 22, 310.00, 0, '2020-02-23'),
(1084, 22, 310.00, 0, '2020-02-24'),
(1085, 22, 310.00, 0, '2020-02-25'),
(1086, 22, 310.00, 0, '2020-02-26'),
(1087, 22, 310.00, 0, '2020-02-27'),
(1088, 22, 310.00, 0, '2020-02-28'),
(1089, 22, 310.00, 0, '2020-02-29'),
(1090, 169, 121.00, 0, '2020-02-19'),
(1091, 169, 121.00, 0, '2020-02-20'),
(1092, 169, 121.00, 0, '2020-02-21'),
(1093, 169, 121.00, 0, '2020-02-22'),
(1094, 176, 130.00, 8, '2020-02-19'),
(1095, 176, 130.00, 8, '2020-02-20'),
(1096, 170, 128.00, 0, '2020-02-19'),
(1097, 170, 128.00, 0, '2020-02-20'),
(1098, 25, 250.00, 0, '2020-02-19'),
(1099, 25, 250.00, 0, '2020-02-20'),
(1100, 24, 130.00, 4, '2020-03-04'),
(1101, 24, 130.00, 4, '2020-03-05'),
(1102, 24, 130.00, 4, '2020-03-06'),
(1103, 24, 130.00, 4, '2020-03-07'),
(1104, 24, 130.00, 4, '2020-03-08'),
(1105, 24, 130.00, 4, '2020-03-09');

-- --------------------------------------------------------

--
-- Table structure for table `tblbintype`
--

CREATE TABLE `tblbintype` (
  `idBinType` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CodeType` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description2` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblbintype`
--

INSERT INTO `tblbintype` (`idBinType`, `name`, `CodeType`, `description`, `description2`) VALUES
(1, 'General Waste', '1210', '<ul>\r\n	<li stlye=\"margin:1px;\">No asbestos or other hazardous material.</li>\r\n	<li stlye=\"margin:1px;\">No hardfill, concrete, sand, soil or bricks.</li>\r\n	<li stlye=\"margin:1px;\">No fibro fencing.</li>\r\n	<li stlye=\"margin:1px;\">No palm trunks, tree trunks roots or turf.</li>\r\n<li stlye=\"margin:1px;\">No wet paint tins.</li>\r\n</ul>', '<ul>\r\n	<li stlye=\"margin:1px;\">Light domestic waste.</li>\r\n	<li stlye=\"margin:1px;\">Light commercial waste.</li>\r\n	<li stlye=\"margin:1px;\">Household items, light office waste, paper and cardboard. </li>\r\n<li stlye=\"margin:1px;\">Furniture and appliances, e.g. cupboards, fridge’s dishwashers etc. </li>\r\n</ul>\r\n\r\n<strong>(NB: Mattresses and car/truck tyres will incur extra charges. If you wish to dispose of these please check with your supplier before placing them in the bin.)</strong>'),
(2, 'Mixed Heavy Waste', '1211', '<ul>\r\n	<li>No asbestos or other hazardous material.</li>\r\n	<li>No food products or food waste.</li>\r\n	<li>Tree trucks larger than 300mm.</li>\r\n</ul>', '<ul>\r\n	<li>Domestic, commercial, construction, renovation and demolition waste.</li>\r\n	<li>domestic, commercial, construction, renovation and demolition.</li>\r\n	<li>All types of appliances, furniture and green waste.</li>\r\n</ul>'),
(3, 'Clean Fills Waste', '1212', '<ul>\r\n	<li>No Asbestos or other hazardous waste</li>\r\n	<li>No Sand, Soil, Clay or Dirt</li>\r\n	<li>No TV sets or computer monitors</li>\r\n	<li>No food products or food waste</li>\r\n</ul>', '<ul>\r\n	<li>Household waste</li>\r\n	<li>Builders waste</li>\r\n	<li>Furniture & appliances</li>\r\n<li>Timber</li>\r\n<li>Bricks, tiles, concrete</li>\r\n<li>Green waste</li>\r\n<li>Metal/steel</li>\r\n</ul>\r\n\r\n<strong>(NB:Mattresses, Carpet, tyres and e-waste may incur extra costs - check with your supplier after booking)</strong>'),
(4, 'Green Waste', '1213', '<ul>\r\n	<li>No asbestos or other hazardous material.</li>\r\n	<li>No bricks, tiles, concrete, sand or soil.</li>\r\n	<li>No paper or cardboard.</li>\r\n	<li>No household waste or appliances.</li>\r\n<li>No food waste.</li>\r\n<li>No mattresses or tyres. </li>\r\n</ul>', '<ul>\r\n	<li>Green garden waste only.</li>\r\n	<li>Branches, leaves, weeds, bark with no soil.</li>\r\n	<li>Shrubs, grass clippings, twigs etc.</li>\r\n<li>Tree trunks smaller than 300mm.</li>\r\n</ul>\r\n\r\n<strong>(NB:This skip must be green waste only.)</strong>'),
(5, 'Soil/Dirt Waste', '1214', '<ul>\r\n	<li>No asbestos or other hazardous material.</li>\r\n	<li>No bricks, tiles, concrete, sand or soil.</li>\r\n	<li>No paper or cardboard.</li>\r\n	<li>No household waste or appliances.</li>\r\n<li>No food waste.</li>\r\n<li>No mattresses or tyres.</li>\r\n</ul>', '<ul>\r\n	<li>Strictly sand and soil only.</li>\r\n</ul>\r\n\r\n<strong>(NB:This skip must be sand/soil only. No other material is allowed.)</strong>');

-- --------------------------------------------------------

--
-- Table structure for table `tblbookingprice`
--

CREATE TABLE `tblbookingprice` (
  `idBookingPrice` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbookingprice`
--

INSERT INTO `tblbookingprice` (`idBookingPrice`, `price`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 4, 5, '2019-05-21 04:11:07', '2019-05-20 18:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `tblcontact`
--

CREATE TABLE `tblcontact` (
  `id` int(11) NOT NULL,
  `first_name` int(11) NOT NULL,
  `last_name` int(11) NOT NULL,
  `email_address` int(11) NOT NULL,
  `delivery_address` int(11) NOT NULL,
  `organization` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblcustomer`
--

CREATE TABLE `tblcustomer` (
  `idCustomer` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `suburb` varchar(100) NOT NULL,
  `zipcode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbldelivery_address`
--

CREATE TABLE `tbldelivery_address` (
  `iddelivery_address` int(11) NOT NULL,
  `unit_number` int(11) NOT NULL,
  `lot_number` int(11) NOT NULL,
  `street_name` text NOT NULL,
  `suburb` varchar(100) NOT NULL,
  `post_code` varchar(5) NOT NULL,
  `state` varchar(100) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `contact` int(11) NOT NULL,
  `idproject_site` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbldepot`
--

CREATE TABLE `tbldepot` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbldepotvisits`
--

CREATE TABLE `tbldepotvisits` (
  `id` int(11) NOT NULL,
  `iddepot` int(11) NOT NULL,
  `departure_type` char(1) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbldrivers`
--

CREATE TABLE `tbldrivers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hours` double NOT NULL,
  `zone` varchar(255) NOT NULL,
  `status` char(1) NOT NULL,
  `licence` varchar(255) NOT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblgeo`
--

CREATE TABLE `tblgeo` (
  `id` int(11) NOT NULL,
  `idtask` int(11) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `departuretype` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbljobcards`
--

CREATE TABLE `tbljobcards` (
  `id` int(11) NOT NULL,
  `jobtype` char(1) NOT NULL,
  `delivery_date` date NOT NULL,
  `end_date` date NOT NULL,
  `orderid` int(11) NOT NULL,
  `status` char(1) NOT NULL,
  `addressid` int(11) NOT NULL,
  `primarycontact` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbllogin_mobile`
--

CREATE TABLE `tbllogin_mobile` (
  `iddriver` int(11) NOT NULL,
  `username` varchar(11) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `permission` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblorderservice`
--

CREATE TABLE `tblorderservice` (
  `idOrderService` int(10) UNSIGNED NOT NULL,
  `idBinService` int(11) NOT NULL,
  `idConsumer` int(11) NOT NULL,
  `idproject_sites` int(11) NOT NULL,
  `paymentUniqueCode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `totalServiceCharge` double(8,2) NOT NULL,
  `gst` double(8,2) NOT NULL,
  `subtotal` double(8,2) NOT NULL,
  `bookingfee` double(8,2) NOT NULL,
  `deliveryComments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idSupplier` int(10) DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `card_category` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_type` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_holder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_number` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tblorderstatus`
--

CREATE TABLE `tblorderstatus` (
  `idOrderStatus` int(11) NOT NULL,
  `idOrder` int(11) NOT NULL,
  `status` char(1) NOT NULL COMMENT '1 = Paid 2= Accepted',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `operator` char(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblorder_items`
--

CREATE TABLE `tblorder_items` (
  `id` int(11) NOT NULL,
  `idorder` int(11) NOT NULL,
  `idbinservice` int(11) NOT NULL,
  `idsize` int(11) NOT NULL,
  `iddelivery_address` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `collection_date` date DEFAULT NULL,
  `subtotal` double NOT NULL,
  `attribute` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblorganization`
--

CREATE TABLE `tblorganization` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `cashonly` char(1) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `mobile_phone` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymentformtemp`
--

CREATE TABLE `tblpaymentformtemp` (
  `idPaymentForm` int(11) NOT NULL,
  `idPaymentTemp` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `suburb` varchar(100) NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `special_note` text DEFAULT NULL,
  `paymentUniqueCode` varchar(20) DEFAULT NULL,
  `totalprice` float DEFAULT NULL,
  `gst` float NOT NULL,
  `subtotal` float NOT NULL,
  `bookingfee` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpaymentformtemp`
--

INSERT INTO `tblpaymentformtemp` (`idPaymentForm`, `idPaymentTemp`, `first_name`, `last_name`, `address`, `suburb`, `zipcode`, `phone`, `email`, `special_note`, `paymentUniqueCode`, `totalprice`, `gst`, `subtotal`, `bookingfee`) VALUES
(296, 416, 'Putu', 'Winata', '7/48 Prindiville Drive Wangara', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test 1', 'N1669989088', 134, 12.1818, 130, 4),
(297, 417, 'Putu', 'Winata', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test 1', '283S6167501', 134, 12.1818, 130, 4),
(298, 418, 'Putu', 'Winata', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test 1', '422228X8191', 134, 12.1818, 130, 4),
(299, 419, 'Putu', 'Winata', 'Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test 1', '913948F6582', 134, 12.1818, 130, 4),
(300, 420, 'Putu', 'Winata', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test 1', '282070366S7', 134, 12.1818, 130, 4),
(301, 421, 'Putu', 'Winata', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test', '418H3745536', 134, 12.1818, 130, 4),
(302, 423, 'Putu', 'Winata', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test', '4M107242912', 134, 12.1818, 130, 4),
(303, 424, 'SOMS', 'Web', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test', '53Y18229706', 134, 12.1818, 130, 4),
(304, 425, 'SOMS', 'Web', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test 1', '742200O7610', 134, 12.1818, 130, 4),
(305, 426, 'Putu', 'Winata', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test 1', '1387197K917', 154, 14, 150, 4),
(306, 427, 'Putu', 'SOMS', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'support@somsweb.com.au', 'Test', '5Z722741786', 154, 14, 150, 4),
(307, 428, 'Bob', 'Bender', '55 buschr Parkway', 'Pearsell', '6065', '30115', 'andrew.soms@gmail.com', NULL, '36B64712604', 134, 12.1818, 130, 4),
(308, 429, 'Sue', 'Small', '36 some place', 'Hocking', '6065', '0429966184', 'andrew.soms@gmail.com', NULL, '3132275407I', 134, 12.1818, 130, 4),
(309, 430, 'Billy', 'Joe', '33 someplace', 'Perth', '6065', '0429966484', 'joe@soms.com.au', 'Make it fast', '2381Q544299', 134, 12.1818, 130, 4),
(310, 432, 'andrew', 'Got', '22 someplace', 'Peaesell', '6065', '015555', 'jim@somsweb.com', 'test', '0564471K156', 134, 12.1818, 130, 4),
(311, 433, 'Putu', 'Winata Test', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', 'Test', '226P5403301', 134, 12.1818, 130, 4),
(312, 434, 'SOMS', 'Test', '7/48 Prindiville Drive Wangara', 'Wangara', '6065', '089658595043', 'support@somsweb.com.au', NULL, '7876101516U', 48, 4.36364, 44, 4),
(313, 435, 'Tutuk', 'SOMS', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', NULL, '1O392216067', 48, 4.36364, 44, 4),
(314, 436, 'SOMS', 'Web', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tutux.hollowbody@gmail.com', NULL, '790G2284860', 154, 14, 150, 4),
(315, 437, 'Steve', 'Boss', '44 test', 'pearsell', '6065', '0022', 'bob@soms.com.au', 'test', '286531121Q9', 134, 12.1818, 130, 4),
(316, 438, 'Brian', 'Setzer', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'tech@somsweb.com.au', NULL, '839769212I3', 134, 12.1818, 130, 4),
(317, 439, 'Mooz', 'kushwaha', '21/266 Near Jawahar Bridge Water Work Agra', 'Aga', '6065', '4147547582', 'yugoxjm@gmail.com', 'test', '21739E07804', 134, 12.1818, 130, 4),
(318, 440, 'Manmohan', 'kushwaha', '21/266 Near Jawahar Bridge Water Work Agra', 'Aga', '6065', '4147547582', 'yugoxjm@gmail.com', 'dasdsa', '0691190K388', 134, 12.1818, 130, 4),
(319, 441, 'Putu Test', 'SOMS Test', '7/48 Prindiville Drive', 'Wangara', '6065', '089658595043', 'support@somsweb.com.au', NULL, '432636Y5721', 134, 12.1818, 130, 4),
(320, 442, 'Nonik', 'SOMS', 'Nusa Dua', '6065', '6065', '089658595043', 'nonik.soms@gmail.com', NULL, '80Z73558869', 134, 12.1818, 130, 4),
(321, 444, 'Nonik', 'SOMS', '55 Nusa Dua', 'Wangara', '6065', '089658595043', 'nonik.soms@gmail.com', NULL, '43U73912607', 134, 12.1818, 130, 4),
(322, 445, 'Mooz', 'Test', '21/266 Near Jawahar Bridge Water Work Agra', 'Aga', '6065', '4147547582', 'autoprix@canadauto.ca', 'test', '92309O31924', 134, 12.1818, 130, 4),
(323, 446, 'Mooz', 'Kushwaha', '21/266 Near Jawahar Bridge Water Work Agra', 'Aga', '6065', '4147547582', 'autoprix@canadauto.ca', 'asdasd', '3M392526958', 134, 12.1818, 130, 4),
(324, 447, 'Mooz', 'Test', '21/266 Near Jawahar Bridge Water Work Agra', 'Aga', '6065', '4147547582', 'superkhojmotiram@gmail.com', 'asdasd', '7318345586U', 226, 20.5455, 222, 4),
(325, 448, 'Mooz', 'Kushwaha', '21/266 Near Jawahar Bridge Water Work Agra', 'Aga', '6065', '4147547582', 'superkhojmotiram@gmail.com', 'testing', '1041335U778', 134, 12.1818, 130, 4),
(326, 449, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', NULL, '71889475Z73', 170, 15.4545, 166, 4),
(327, 450, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', 'adasd', 'H6292735310', 314, 28.5455, 310, 4),
(328, 451, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', NULL, '7T475046850', 125, 11.3636, 121, 4),
(329, 452, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', NULL, '0Q863937943', 134, 12.1818, 130, 4),
(330, 453, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', NULL, '81188953M41', 154, 14, 150, 4),
(331, 454, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', 'test', '846870N1736', 132, 12, 128, 4),
(332, 455, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', NULL, '77Z23789666', 254, 23.0909, 250, 4),
(333, 456, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', NULL, '9972772O233', 134, 12.1818, 130, 4),
(334, 457, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'kushwahamanmohan51@gmail.com', NULL, '123K7323410', 134, 12.1818, 130, 4),
(335, 458, 'Moti', 'Ram', 'Behind Gaushala', 'Agra', '6065', '07417300994', 'superkhojmotiram@gmail.com', 'test', '523456X9991', 134, 12.1818, 130, 4),
(336, 459, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'saandy87@gmail.com', 'Testing', '26861J69422', 154, 14, 150, 4),
(337, 460, 'Chanchal', 'Agarwal', '61, Inder Enclave', 'Agar', '6065', '08909010007', 'withmanmohan@gmail.com', NULL, '321823796P1', 254, 23.0909, 250, 4),
(338, 461, 'New Bin', 'March2020', '55 Someplace', 'Perth', '6065', '0429966484', 'andrew.soms@gmail.com', NULL, '05386E87594', 134, 12.1818, 130, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tblpaymenttemp`
--

CREATE TABLE `tblpaymenttemp` (
  `idPaymentTemp` int(11) NOT NULL,
  `idBinHire` int(11) NOT NULL,
  `deliveryDate` date NOT NULL,
  `collectionDate` date NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  `paid` char(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpaymenttemp`
--

INSERT INTO `tblpaymenttemp` (`idPaymentTemp`, `idBinHire`, `deliveryDate`, `collectionDate`, `zipcode`, `paid`) VALUES
(416, 22, '2020-02-15', '2020-02-16', '6065', '0'),
(417, 22, '2020-02-15', '2020-02-16', '6065', '1'),
(418, 23, '2020-02-17', '2020-02-18', '6065', '1'),
(419, 23, '2020-02-18', '2020-02-19', '6065', '1'),
(420, 23, '2020-02-20', '2020-02-21', '6065', '1'),
(421, 23, '2020-02-15', '2020-02-16', '6065', '1'),
(422, 23, '2020-02-15', '2020-02-16', '6065', '0'),
(423, 24, '2020-02-24', '2020-02-25', '6065', '1'),
(424, 23, '2020-03-02', '2020-03-03', '6065', '1'),
(425, 24, '2020-03-11', '2020-03-12', '6065', '1'),
(426, 171, '2020-03-09', '2020-03-11', '6065', '1'),
(427, 171, '2020-02-16', '2020-02-17', '6065', '1'),
(428, 22, '2020-02-16', '2020-02-17', '6065', '1'),
(429, 22, '2020-02-17', '2020-02-18', '6065', '1'),
(430, 22, '2020-02-19', '2020-02-20', '6065', '1'),
(431, 169, '2020-02-19', '2020-02-20', '6065', '0'),
(432, 22, '2020-02-19', '2020-02-20', '6065', '1'),
(433, 22, '2020-03-02', '2020-03-03', '6065', '1'),
(434, 26, '2020-02-19', '2020-02-20', '6065', '1'),
(435, 26, '2020-02-19', '2020-02-20', '6065', '1'),
(436, 171, '2020-02-19', '2020-02-20', '6065', '1'),
(437, 22, '2020-02-19', '2020-02-20', '6065', '1'),
(438, 24, '2020-02-19', '2020-02-20', '6065', '1'),
(439, 22, '2020-02-19', '2020-02-20', '6065', '0'),
(440, 24, '2020-02-19', '2020-02-20', '6065', '1'),
(441, 22, '2020-02-19', '2020-02-20', '6065', '1'),
(442, 175, '2020-02-19', '2020-02-20', '6065', '1'),
(443, 175, '2020-02-19', '2020-02-20', '6065', '0'),
(444, 175, '2020-02-19', '2020-02-20', '6065', '1'),
(445, 22, '2020-02-19', '2020-02-20', '6065', '1'),
(446, 175, '2020-02-19', '2020-02-20', '6065', '1'),
(447, 57, '2020-02-19', '2020-02-20', '6065', '1'),
(448, 22, '2020-02-19', '2020-02-20', '6065', '1'),
(449, 172, '2020-02-19', '2020-02-29', '6065', '1'),
(450, 22, '2020-02-19', '2020-02-29', '6065', '1'),
(451, 169, '2020-02-19', '2020-02-22', '6065', '1'),
(452, 176, '2020-02-19', '2020-02-20', '6065', '1'),
(453, 171, '2020-02-19', '2020-02-20', '6065', '1'),
(454, 170, '2020-02-19', '2020-02-20', '6065', '1'),
(455, 25, '2020-02-19', '2020-02-20', '6065', '1'),
(456, 24, '2020-02-19', '2020-02-20', '6065', '1'),
(457, 24, '2020-02-19', '2020-02-20', '6065', '1'),
(458, 176, '2020-02-19', '2020-02-20', '6065', '1'),
(459, 171, '2020-02-19', '2020-02-20', '6065', '1'),
(460, 25, '2020-02-19', '2020-02-20', '6065', '1'),
(461, 24, '2020-03-04', '2020-03-09', '6065', '1'),
(462, 24, '2020-03-04', '2020-03-12', '6065', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tblproject_sites`
--

CREATE TABLE `tblproject_sites` (
  `id` int(11) NOT NULL,
  `idorganization` int(11) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `gate` varchar(100) NOT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblservicearea`
--

CREATE TABLE `tblservicearea` (
  `idArea` int(11) NOT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `area` text DEFAULT NULL,
  `parentareacode` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblservicearea`
--

INSERT INTO `tblservicearea` (`idArea`, `zipcode`, `area`, `parentareacode`) VALUES
(1, '0000', 'Western Australia', '0'),
(2, '0001', 'City of Armadale', '0000'),
(3, '0002', 'Town of Bassendean', '0000'),
(4, '0003', 'City of Bayswater', '0000'),
(5, '0004', 'City of Belmont', '0000'),
(6, '0005', 'Town of Cambridge', '0000'),
(7, '0006', 'City of Canning', '0000'),
(9, '0008', 'City of Cockburn', '0000'),
(10, '0009', 'Town of Cottesloe', '0000'),
(11, '0010', 'Town of East Fremantle', '0000'),
(12, '0011', 'City of Fremantle', '0000'),
(13, '0012', 'City of Gosnells', '0000'),
(14, '0013', 'City of Joondalup', '0000'),
(15, '0014', 'City of Kalamunda', '0000'),
(16, '0015', 'City of Kwinana', '0000'),
(17, '0016', 'City of Melville', '0000'),
(18, '0017', 'Town of Mosman Park', '0000'),
(20, '0019', 'City of Nedlands', '0000'),
(22, '0021', 'City of Perth', '0000'),
(23, '0022', 'City of Rockingham', '0000'),
(24, '0023', 'Shire of Serpentine-Jarrahdale', '0000'),
(25, '0024', 'City of South Perth', '0000'),
(26, '0025', 'City of Stirling', '0000'),
(28, '0027', 'City of Swan', '0000'),
(29, '0028', 'Town of Victoria Park', '0000'),
(30, '0029', 'City of Vincent', '0000'),
(31, '0030', 'City of Wanneroo', '0000'),
(32, '0031', 'City of Burnburry', '0000'),
(33, '6111', 'Ashendon, Canning Mills, Champion Lakes, Karragullen, Kelmscott, Lesley, Roleystone, Westfield', '0001'),
(34, '6112', 'Armadale, Bedfordale, Brookdale, Forrestdale, Mount Nasura, Mount Richon, Seville Grove, Wungong', '0001'),
(38, '6051', 'Maylands', '0003'),
(39, '6053', 'Bayswater', '0003'),
(40, '6062', 'Embleton, Morley, Noranda', '0003'),
(43, '6103', 'Rivervale', '0004'),
(44, '6104', 'Ascot, Belmont, Redcliffe', '0004'),
(45, '6105', 'Cloverdale, Kewdale, Perth Airport', '0004'),
(48, '6230', 'Bunbury, Carey Park, College Grove, Dalyellup, Davenport, East Bunbury, Gelorup, Glen Iris, Pelican Point, South Bunbury, Usher, Vittoria, Withers', '0031'),
(50, '6102', 'Bentley, St James', '0006'),
(51, '6106', 'Welshpool', '0006'),
(52, '6107', 'Beckenham, Cannington, Kenwick, Queens Park, Wattle Grove, Wilson', '0006'),
(53, '6147', 'Langford, Lynwood, Parkwood', '0006'),
(54, '6148', 'Ferndale, Riverton, Rossmoyne, Shelley', '0006'),
(55, '6149', 'Bull Creek, Leeming', '0006'),
(56, '6155', 'Canning Vale, Willetton', '0006'),
(63, '6163', 'Bibra Lake, Coolbellup, Hamilton Hill, Hilton, Kardinya, North Coogee, North Lake, O Connor, Samson, Spearwood', '0008'),
(64, '6164', 'Atwell, Aubin Grove, Banjup, Beeliar, Cockburn Central, Hammond Park, Jandakot, South Lake, Success, Yangebup', '0008'),
(65, '6166', 'Coogee, Henderson, Munster, Wattleup', '0008'),
(69, '6159', 'North Fremantle', '0011'),
(70, '6160', 'Fremantle', '0011'),
(71, '6162', 'Beaconsfield, South Fremantle, White Gum Valley', '0011'),
(73, '6108', 'Thornlie', '0012'),
(74, '6109', 'Maddington, Orange Grove', '0012'),
(75, '6110', 'Gosnells, Huntingdale, Martin, Southern River', '0012'),
(80, '6023', 'Duncraig', '0013'),
(81, '6024', 'Greenwood, Warwick', '0013'),
(83, '6025', 'Craigie, Hillarys, Kallaroo, Padbury', '0013'),
(84, '6027', 'Beldon, Connolly, Edgewater, Heathridge, Joondalup, Mullaloo, Ocean Reef', '0013'),
(85, '6028', 'Burns Beach, Currambine, Iluka, Kinross', '0013'),
(88, '6020', 'Carine, Marmion, North Beach, Sorrento, Watermans Bay', '0013'),
(89, '6057', 'High Wycombe, Maida Vale', '0014'),
(90, '6076', 'Bickley, Carmel, Gooseberry Hill, Hacketts Gully, Kalamunda, Lesmurdie, Paulls Valley, Pickering Brook, Piesse Brook, Reservoir, Walliston', '0014'),
(93, '6058', 'Forrestfield', '0014'),
(94, '6165', 'Hope Valley, Naval Base', '0015'),
(95, '6167', 'Anketell, Bertram, Calista, Casuarina, Kwinana Beach, Kwinana Town Centre, Mandogalup, Medina, Orelia, Parmelia, Postans, The Spectacles, Wandi', '0015'),
(96, '6170', 'Leda, Wellard', '0015'),
(98, '6714', 'Antonymyre, Balla Balla, Baynton, Bulgarra, Burrup, Cleaverville, Cooya Pooya, Gap Ridge, Gnoorea, Karratha, Karratha Industrial Estate, Maitland, Mardie, Millars Well, Mount Anketell, Mulataga, Nickol, Pegs Creek, Sherlock, Stove Hill', '0015'),
(99, '0032', 'City of Mandurah', '0000'),
(100, '6180', 'Lakelands, Parklands', '0032'),
(101, '6181', 'Stake Hill', '0032'),
(102, '6207', 'Myara, Nambeelup, North Dandalup, Solus, Whittaker', '0032'),
(103, '6208', 'Blythewood, Fairbridge, Meelon, Nirimba, North Yunderup, Oakley, Pinjarra, Point Grey, Ravenswood, South Yunderup, West Pinjarra', '0032'),
(104, '6209', 'Barragup, Furnissdale', '0032'),
(105, '6210', 'Coodanup, Dudley Park, Erskine, Falcon, Greenfields, Halls Head, Madora Bay, Mandurah, Meadow Springs, San Remo, Silver Sands, Wannanup', '0032'),
(106, '6211', 'Bouvard, Clifton, Dawesville, Herron', '0032'),
(107, '6213', 'Banksiadale, Dwellingup, Etmilyn, Holyoake, Inglehope, Marrinup, Teesdale', '0032'),
(108, '6214', 'Birchmont, Coolup', '0032'),
(109, '6150', 'Bateman, Murdoch, Winthrop', '0016'),
(110, '6153', 'Applecross, Ardross, Brentwood, Mount Pleasant', '0016'),
(111, '6154', 'Alfred Cove, Booragoon, Myaree', '0016'),
(112, '6156', 'Attadale, Melville, Willagee', '0016'),
(113, '6157', 'Bicton, Palmyra', '0016'),
(120, '6008', 'Daglish, Shenton Park, Subiaco', '0019'),
(121, '6907', 'Nedlands', '0019'),
(124, '6000', 'Perth', '0021'),
(125, '6003', 'Highgate, Northbridge', '0021'),
(126, '6004', 'East Perth', '0021'),
(127, '6005', 'Kings Park, West Perth', '0021'),
(128, '6009', 'Crawley, Dalkeith, Nedlands', '0021'),
(147, '6168', 'Cooloongup, East Rockingham, Garden Island, Hillman, Peron, Rockingham', '0022'),
(148, '6169', 'Safety Bay, Shoalwater, Waikiki, Warnbro', '0022'),
(149, '6172', 'Baldivis', '0022'),
(150, '6172', 'Port Kennedy', '0022'),
(151, '6173', 'Secret Harbour', '0022'),
(152, '6174', 'Golden Bay', '0022'),
(153, '6175', 'Singleton', '0022'),
(154, '6176', 'Karnup', '0022'),
(159, '6152', 'Como, Karawara, Manning, Salter Point, Waterford', '0024'),
(163, '6022', 'Hamersley', '0025'),
(165, '6016', 'Glendalough, Mount Hawthorn', '0025'),
(166, '6017', 'Herdsman, Osborne Park', '0025'),
(167, '6018', 'Churchlands, Doubleview, Gwelup, Innaloo, Karrinyup, Woodlands', '0025'),
(168, '6019', 'Scarborough, Wembley Downs', '0025'),
(169, '6021', 'Balcatta, Stirling', '0025'),
(170, '6029', 'Trigg', '0025'),
(171, '6052', 'Bedford, Inglewood', '0025'),
(172, '6060', 'Joondanna, Tuart Hill, Yokine', '0025'),
(183, '6059', 'Dianella', '0025'),
(184, '6061', 'Balga, Mirrabooka, Nollamara, Westminster', '0025'),
(186, '6055', 'Caversham, Guildford, Hazelmere, Henley Brook, South Guildford, West Swan', '0027'),
(187, '6056', 'Baskerville, Bellevue, Boya, Greenmount, Helena Valley, Herne Hill, Jane Brook, Koongamia, Middle Swan, Midland, Midvale, Millendon, Red Hill, Stratton, Swan View, Viveash, Woodbridge', '0027'),
(188, '6063', 'Beechboro', '0027'),
(189, '6067', 'Cullacabardee', '0027'),
(190, '6068', 'Whiteman', '0027'),
(191, '6069', 'Aveley, Belhus, Brigadoon, Ellenbrook, The Vines, Upper Swan', '0027'),
(192, '6083', 'Gidgegannup, Morangup', '0027'),
(193, '6084', 'Bullsbrook, Chittering, Lower Chittering', '0027'),
(194, '6066', 'Ballajura', '0027'),
(195, '6090', 'Malaga', '0027'),
(199, '0035', 'Shire of Gingin', '0000'),
(200, '0036', 'Shire of Harvey', '0000'),
(201, '0037', 'Shire of Mundaring', '0000'),
(205, '6005', 'West Perth', '0029'),
(206, '6006', 'North Perth', '0029'),
(207, '6050', 'Coolbinia, Menora, Mount Lawley', '0029'),
(212, '6026', 'Kingsley, Woodvale', '0030'),
(213, '6065', 'Ashby, Darch, Hocking, Landsdale, Lexia, Madeley, Melaleuca, Pearsall, Sinagra, Tapping, Wangara, Wanneroo', '0030'),
(214, '6077', 'Gnangara, Jandabup', '0030'),
(215, '6078', 'Mariginiup, Pinjar', '0030'),
(218, '6030', 'Clarkson, Merriwa, Mindarie, Quinns Rocks, Ridgewood, Tamala Park', '0030'),
(219, '6031', 'Banksia Grove, Carramar, Neerabup', '0030'),
(220, '6032', 'Nowergup', '0030'),
(221, '6033', 'Carabooda', '0030'),
(222, '6034', 'Eglinton', '0030'),
(223, '6035', 'Yanchep', '0030'),
(224, '6036', 'Butler, Jindalee', '0030'),
(225, '6037', 'Two Rocks', '0030'),
(226, '6038', 'Alkimos', '0030'),
(227, '6064', 'Alexander Heights, Girrawheen, Koondoola, Marangaroo', '0030'),
(230, '6041', 'Caraban, Gabbadah, Guilderton, Wilbinga, Woodridge', '0035'),
(231, '6042', 'Seabird', '0035'),
(232, '6043', 'Breton Bay, Ledge Point', '0035'),
(233, '6044', 'Karakin, Lancelin, Nilgen, Wedge Island', '0035'),
(234, '6233', 'Australind, Binningup, Leschenault, Parkfield, Wellesley', '0036'),
(235, '6556', 'Beechina, Chidlow, Gorrie, Malmalling, The Lakes', '0037'),
(236, '6558', 'Wooroloo', '0037'),
(237, '6070', 'Darlington', '0037'),
(238, '6071', 'Glen Forrest, Hovea', '0037'),
(239, '6072', 'Mahogany Creek', '0037'),
(240, '6073', 'Mundaring', '0037'),
(241, '6074', 'Sawyers Valley', '0037'),
(242, '6081', 'Parkerville, Stoneville', '0037'),
(243, '6082', 'Mount Helena', '0037'),
(245, '6122', 'Byford, Cardup, Darling Downs, Karrakup', '0023'),
(246, '6124', 'Jarrahdale', '0023'),
(247, '6125', 'Hopeland, Mardella, Serpentine', '0023'),
(248, '6126', 'Keysbrook', '0023'),
(249, '6121', 'Oakford, Oldbury', '0023'),
(250, '6123', 'Mundijong, Whitby', '0023'),
(253, '6054', 'Ashfield, Bassendean, Eden Hill, Kiara, Lockridge', '0002'),
(256, '6010', 'Claremont, Karrakatta, Mount Claremont, Swanbourne', '0005'),
(257, '6007', 'Leederville, West Leederville', '0005'),
(258, '6014', 'Floreat, Jolimont, Wembley', '0005'),
(259, '6015', 'City Beach', '0005'),
(262, '6011', 'Cottesloe, Peppermint Grove', '0009'),
(263, '6158', 'East Fremantle', '0010'),
(264, '6012', 'Mosman Park', '0017'),
(268, '6100', 'Burswood, Lathlain, Victoria Park', '0028'),
(269, '6101', 'Carlisle, East Victoria Park', '0028'),
(270, '6151', 'Kensington, South Perth', '0028'),
(271, '6026', 'Joondalup', '0013');

-- --------------------------------------------------------

--
-- Table structure for table `tblsize`
--

CREATE TABLE `tblsize` (
  `idSize` int(10) UNSIGNED NOT NULL,
  `size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dimensions` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblsize`
--

INSERT INTO `tblsize` (`idSize`, `size`, `dimensions`) VALUES
(1, '2 cubic metres', '2 Cubic Metres Skip is about the same capacity as 2 standard box trailers'),
(2, '3 cubic metres', '3 Cubic Metres Skip is about the same capacity as 3 standard box trailers'),
(3, '4 cubic metres', '4 Cubic Metre Skip is about the same capacity as 4 standard box trailerss'),
(4, '5 cubic metres', '5 Cubic Metre Skip is about the same capacity as 5 standard box trailers'),
(5, '6 cubic metres', '6 Cubic Metre Skip is about the same capacity as 6 standard box trailers'),
(6, '8 cubic metres', '8 Cubic Metres Skip is about the same capacity as 8 standard box trailers'),
(7, '10 cubic metres', '10 Cubic Metres Skip is about the same capacity as 12 standard box trailers'),
(10, '12 cubic metres', '12 Cubic Metres Skip is about the same capacity as 12 standard box trailers');

-- --------------------------------------------------------

--
-- Table structure for table `tblsupplier`
--

CREATE TABLE `tblsupplier` (
  `idSupplier` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobilePhone` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isOpenSaturday` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isOpenSunday` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mainServiceArea` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServiceContact` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServicePhone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customerServiceMobile` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullAddress` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `abn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tblsupplier`
--

INSERT INTO `tblsupplier` (`idSupplier`, `name`, `contactName`, `phonenumber`, `email`, `mobilePhone`, `comments`, `email2`, `isOpenSaturday`, `isOpenSunday`, `mainServiceArea`, `customerServiceContact`, `customerServicePhone`, `customerServiceMobile`, `fullAddress`, `abn`) VALUES
(11, 'Webmaster', 'iHub', '0361241875', 'andrew.soms@gmail.com', '089658595043', 'test', 'andrew.soms@gmail.com', '1', '1', '2', 'Support', '0361 241822', '089658595066', 'Joondalup 6027', '44 331 419 320');

-- --------------------------------------------------------

--
-- Table structure for table `tbltasks`
--

CREATE TABLE `tbltasks` (
  `id` int(11) NOT NULL,
  `idjobcard` int(11) NOT NULL,
  `starttime` timestamp NOT NULL DEFAULT current_timestamp(),
  `endtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `assignedto` int(11) NOT NULL,
  `taskname` varchar(255) NOT NULL,
  `status` char(1) NOT NULL,
  `note` text NOT NULL,
  `geo_onsite` varchar(255) NOT NULL,
  `geo_start` varchar(255) NOT NULL,
  `actual_tasktime` timestamp NOT NULL DEFAULT current_timestamp(),
  `km` double NOT NULL,
  `actual_endtime` timestamp NOT NULL DEFAULT current_timestamp(),
  `orderitem` int(11) NOT NULL,
  `assets` int(11) NOT NULL,
  `driver` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbltrucks`
--

CREATE TABLE `tbltrucks` (
  `id` int(11) NOT NULL,
  `truck_type` varchar(100) NOT NULL,
  `in_service` char(1) NOT NULL,
  `load_weight` double NOT NULL,
  `name` varchar(255) NOT NULL,
  `registration` varchar(100) NOT NULL,
  `lift_type` varchar(100) NOT NULL,
  `lift_weight` varchar(100) NOT NULL,
  `next_service_date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltrucks`
--

INSERT INTO `tbltrucks` (`id`, `truck_type`, `in_service`, `load_weight`, `name`, `registration`, `lift_type`, `lift_weight`, `next_service_date`) VALUES
(1, '', '', 0, '', '', '', '', NULL),
(2, '', '', 0, '', '', '', '', NULL),
(3, '', '', 0, '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `idUser` int(10) UNSIGNED NOT NULL,
  `idSupplier` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminApproved` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userStatus` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registerDate` date NOT NULL,
  `adminNotifications` char(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`idUser`, `idSupplier`, `username`, `password`, `isActive`, `role`, `adminApproved`, `userStatus`, `registerDate`, `adminNotifications`) VALUES
(5, 11, 'webmaster', '62c8ad0a15d9d1ca38d5dee762a16e01', '1', '1', '1', '1', '2018-06-01', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbluserservicearea`
--

CREATE TABLE `tbluserservicearea` (
  `idUserServiceArea` int(10) NOT NULL,
  `idUser` int(10) NOT NULL,
  `idSupplier` int(10) NOT NULL,
  `idServiceArea` int(10) NOT NULL,
  `serviceAreaParent` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbluserservicearea`
--

INSERT INTO `tbluserservicearea` (`idUserServiceArea`, `idUser`, `idSupplier`, `idServiceArea`, `serviceAreaParent`) VALUES
(815, 5, 11, 33, '0001'),
(816, 5, 11, 34, '0001'),
(817, 5, 11, 38, '0003'),
(818, 5, 11, 39, '0003'),
(819, 5, 11, 40, '0003'),
(820, 5, 11, 80, '0013'),
(821, 5, 11, 81, '0013'),
(822, 5, 11, 83, '0013'),
(823, 5, 11, 84, '0013'),
(824, 5, 11, 85, '0013'),
(825, 5, 11, 88, '0013'),
(826, 5, 11, 271, '0013'),
(827, 5, 11, 212, '0030'),
(828, 5, 11, 213, '0030'),
(829, 5, 11, 214, '0030'),
(830, 5, 11, 215, '0030'),
(831, 5, 11, 218, '0030'),
(832, 5, 11, 219, '0030'),
(833, 5, 11, 220, '0030'),
(834, 5, 11, 221, '0030'),
(835, 5, 11, 222, '0030'),
(836, 5, 11, 223, '0030'),
(837, 5, 11, 224, '0030'),
(838, 5, 11, 225, '0030'),
(839, 5, 11, 226, '0030'),
(840, 5, 11, 227, '0030'),
(841, 5, 11, 124, '0021'),
(842, 5, 11, 125, '0021'),
(843, 5, 11, 126, '0021'),
(844, 5, 11, 127, '0021'),
(845, 5, 11, 128, '0021');

-- --------------------------------------------------------

--
-- Table structure for table `user_confirmation`
--

CREATE TABLE `user_confirmation` (
  `idConfirmation` int(11) NOT NULL,
  `idSupplier` int(11) DEFAULT NULL,
  `idUser` int(11) DEFAULT NULL,
  `token` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `csv_data`
--
ALTER TABLE `csv_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblbinnondelivery`
--
ALTER TABLE `tblbinnondelivery`
  ADD PRIMARY KEY (`idNonDeliveryDays`);

--
-- Indexes for table `tblbinservice`
--
ALTER TABLE `tblbinservice`
  ADD PRIMARY KEY (`idBinService`);

--
-- Indexes for table `tblbinserviceoptions`
--
ALTER TABLE `tblbinserviceoptions`
  ADD PRIMARY KEY (`idBinServiceOptions`);

--
-- Indexes for table `tblbinservicestok`
--
ALTER TABLE `tblbinservicestok`
  ADD PRIMARY KEY (`idBinServiceStok`);

--
-- Indexes for table `tblbinserviceupdates`
--
ALTER TABLE `tblbinserviceupdates`
  ADD PRIMARY KEY (`idBinServiceUpdates`);

--
-- Indexes for table `tblbintype`
--
ALTER TABLE `tblbintype`
  ADD PRIMARY KEY (`idBinType`);

--
-- Indexes for table `tblbookingprice`
--
ALTER TABLE `tblbookingprice`
  ADD PRIMARY KEY (`idBookingPrice`);

--
-- Indexes for table `tblcontact`
--
ALTER TABLE `tblcontact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  ADD PRIMARY KEY (`idCustomer`);

--
-- Indexes for table `tbldelivery_address`
--
ALTER TABLE `tbldelivery_address`
  ADD PRIMARY KEY (`iddelivery_address`);

--
-- Indexes for table `tbldepot`
--
ALTER TABLE `tbldepot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbldrivers`
--
ALTER TABLE `tbldrivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblgeo`
--
ALTER TABLE `tblgeo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbljobcards`
--
ALTER TABLE `tbljobcards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblorderservice`
--
ALTER TABLE `tblorderservice`
  ADD PRIMARY KEY (`idOrderService`);

--
-- Indexes for table `tblorderstatus`
--
ALTER TABLE `tblorderstatus`
  ADD PRIMARY KEY (`idOrderStatus`);

--
-- Indexes for table `tblorder_items`
--
ALTER TABLE `tblorder_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblorganization`
--
ALTER TABLE `tblorganization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpaymentformtemp`
--
ALTER TABLE `tblpaymentformtemp`
  ADD PRIMARY KEY (`idPaymentForm`);

--
-- Indexes for table `tblpaymenttemp`
--
ALTER TABLE `tblpaymenttemp`
  ADD PRIMARY KEY (`idPaymentTemp`);

--
-- Indexes for table `tblproject_sites`
--
ALTER TABLE `tblproject_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblservicearea`
--
ALTER TABLE `tblservicearea`
  ADD PRIMARY KEY (`idArea`);

--
-- Indexes for table `tblsize`
--
ALTER TABLE `tblsize`
  ADD PRIMARY KEY (`idSize`);

--
-- Indexes for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  ADD PRIMARY KEY (`idSupplier`);

--
-- Indexes for table `tbltasks`
--
ALTER TABLE `tbltasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbltrucks`
--
ALTER TABLE `tbltrucks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`idUser`);

--
-- Indexes for table `tbluserservicearea`
--
ALTER TABLE `tbluserservicearea`
  ADD PRIMARY KEY (`idUserServiceArea`);

--
-- Indexes for table `user_confirmation`
--
ALTER TABLE `user_confirmation`
  ADD PRIMARY KEY (`idConfirmation`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `csv_data`
--
ALTER TABLE `csv_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblbinnondelivery`
--
ALTER TABLE `tblbinnondelivery`
  MODIFY `idNonDeliveryDays` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=806;

--
-- AUTO_INCREMENT for table `tblbinservice`
--
ALTER TABLE `tblbinservice`
  MODIFY `idBinService` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `tblbinserviceoptions`
--
ALTER TABLE `tblbinserviceoptions`
  MODIFY `idBinServiceOptions` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `tblbinservicestok`
--
ALTER TABLE `tblbinservicestok`
  MODIFY `idBinServiceStok` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=989;

--
-- AUTO_INCREMENT for table `tblbinserviceupdates`
--
ALTER TABLE `tblbinserviceupdates`
  MODIFY `idBinServiceUpdates` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1106;

--
-- AUTO_INCREMENT for table `tblbintype`
--
ALTER TABLE `tblbintype`
  MODIFY `idBinType` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblbookingprice`
--
ALTER TABLE `tblbookingprice`
  MODIFY `idBookingPrice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblcontact`
--
ALTER TABLE `tblcontact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcustomer`
--
ALTER TABLE `tblcustomer`
  MODIFY `idCustomer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `tbldelivery_address`
--
ALTER TABLE `tbldelivery_address`
  MODIFY `iddelivery_address` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbldepot`
--
ALTER TABLE `tbldepot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbldrivers`
--
ALTER TABLE `tbldrivers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblgeo`
--
ALTER TABLE `tblgeo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbljobcards`
--
ALTER TABLE `tbljobcards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblorderservice`
--
ALTER TABLE `tblorderservice`
  MODIFY `idOrderService` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `tblorderstatus`
--
ALTER TABLE `tblorderstatus`
  MODIFY `idOrderStatus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=237;

--
-- AUTO_INCREMENT for table `tblorder_items`
--
ALTER TABLE `tblorder_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblorganization`
--
ALTER TABLE `tblorganization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpaymentformtemp`
--
ALTER TABLE `tblpaymentformtemp`
  MODIFY `idPaymentForm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=339;

--
-- AUTO_INCREMENT for table `tblpaymenttemp`
--
ALTER TABLE `tblpaymenttemp`
  MODIFY `idPaymentTemp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=463;

--
-- AUTO_INCREMENT for table `tblproject_sites`
--
ALTER TABLE `tblproject_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblservicearea`
--
ALTER TABLE `tblservicearea`
  MODIFY `idArea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=272;

--
-- AUTO_INCREMENT for table `tblsize`
--
ALTER TABLE `tblsize`
  MODIFY `idSize` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  MODIFY `idSupplier` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `tbltasks`
--
ALTER TABLE `tbltasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbltrucks`
--
ALTER TABLE `tbltrucks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `idUser` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tbluserservicearea`
--
ALTER TABLE `tbluserservicearea`
  MODIFY `idUserServiceArea` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=846;

--
-- AUTO_INCREMENT for table `user_confirmation`
--
ALTER TABLE `user_confirmation`
  MODIFY `idConfirmation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
