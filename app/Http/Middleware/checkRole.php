<?php
namespace App\Http\Middleware;
use Closure;
class checkRole
{
	/**
	* Handle an incoming request.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  \Closure  $next
	* @return mixed
	*/
	public function handle($request, Closure $next, ...$roles)
	{
		if (!$request->session()->has('role')){
			$request->session()->flash('access_violated_status', 'danger');
			$request->session()->flash('access_violated_message', 'Session expired. Please Log in again');
			return redirect('login');
		} else{
			foreach($roles as $role) {
				if($request->session()->get('role') == $role){
					return $next($request);
				}
			}
		}
		$request->session()->flash('access_violated_status', 'danger');
		$request->session()->flash('access_violated_message', 'Your role is limited to that page');
		return redirect('/');
	}
}
