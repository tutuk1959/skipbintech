<?php

namespace App\Http\Middleware;
use Closure;

class SignedIn
{
	/**
	* Handle an incoming request.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  \Closure  $next
	* @return mixed
	*/
	public function handle($request, Closure $next)
	{
		if (!$request->session()->has('role')){
			$request->session()->flash('access_violated_status', 'danger');
			$request->session()->flash('access_violated_message', 'Session expired. Please Log in again');
			return redirect('/');
		} else{
			$request->session()->flash('access_violated_status', 'danger');
			$request->session()->flash('access_violated_message', 'Your role is limited to that page');
			return redirect('instructions')->with('status', 'warning');
		}
		
	}
}
