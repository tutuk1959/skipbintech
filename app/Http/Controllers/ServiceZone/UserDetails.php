<?php

namespace App\Http\Controllers\ServiceZone;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\tblservicearea;
use App\tblsupplier;
use App\tbluse;
use App\tbluserservicearea;
use App\tblbookingservice;
use App\CsvData;
use Illuminate\Support\Facades\DB;
use Validator;
use Redirect;
use App\Http\Requests\CsvImportRequest;
use Maatwebsite\Excel\Facades\Excel;

class UserDetails extends Controller
{
	/** Display Listing **/
	public function index($result = null){
		$idUser = 	session('idUser');
		$childArea = null;
		$childServiceArea = null;
		$mainServiceArea = DB::table('tbluser')
			->leftJoin('tblsupplier', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblservicearea', 'tblsupplier.mainServiceArea', '=' , 'tblservicearea.idArea')
			->select('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblservicearea.zipcode', 'tblservicearea.area'
				,'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.phonenumber')
			->first();
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress','tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2', 
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact', 
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress','tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		$serviceAreaParent = DB::table('tblservicearea')
							->select('idArea', 'zipcode','area', 'parentareacode')
							->where(['parentareacode' => '0000'])
							->orderBy('area','asc')
							->get();
		
		if(!is_null($serviceAreaParent)){
			foreach ($serviceAreaParent as $row){
				$childArea[$row->idArea][$row->zipcode] = DB::table('tblservicearea')
					->select('idArea', 'zipcode','area', 'parentareacode')
					->where(['parentareacode' => $row->zipcode])
					->get();
			}
			
			if(!is_null($childArea)){
				foreach($childArea as $ca){
					foreach ($ca as $sca){
						foreach ($sca as $ssca){
							$childServiceArea[$ssca->parentareacode][$ssca->idArea]= DB::table('tbluserservicearea')
									->select('idUser', 'idSupplier','idServiceArea', 'serviceAreaParent')
									->where(['idUser' => $idUser])
									->get();
						}
					}
				}
				
			}
			return view('detail_service_area', ['supplierData' => $supplierData, 'mainServiceArea'=> $mainServiceArea,'serviceAreaParent' => $serviceAreaParent, 
			'childArea' => $childArea, 'childServiceArea' => $childServiceArea,'result' => $result]);
		} else {
			return view('detail_service_area', ['supplierData' => $supplierData, 'mainServiceArea'=> $mainServiceArea,'serviceAreaParent' => $serviceAreaParent, 
			'childArea' => $childArea, 'childServiceArea' => $childServiceArea,'result' => $result]);
		}
	}
}
